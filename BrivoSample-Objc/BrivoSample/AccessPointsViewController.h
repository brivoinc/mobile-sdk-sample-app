//
//  AccessPointsViewController.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 29/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class BrivoOnairPass;
@interface AccessPointsViewController : UIViewController
@property (strong, nonatomic) BrivoOnairPass *brivoOnAirPass;
@property (nonatomic) NSInteger selectedSiteIndex;
@end

NS_ASSUME_NONNULL_END
