//
//  LabelTableViewCell.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 29/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LabelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
