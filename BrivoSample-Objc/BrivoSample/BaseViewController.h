//
//  BaseViewController.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 04/11/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController
- (void) showAlertWith: (NSString *)title :(NSString *) message;
@end

NS_ASSUME_NONNULL_END
