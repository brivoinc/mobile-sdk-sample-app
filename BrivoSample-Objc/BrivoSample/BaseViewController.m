//
//  BaseViewController.m
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 04/11/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void) showAlertWith: (NSString *)title :(NSString *) message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle: title message: message preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style: UIAlertActionStyleDefault handler: nil];

    [alertController addAction: okAction];
    [self presentViewController:alertController animated: true completion: nil];
}
@end
