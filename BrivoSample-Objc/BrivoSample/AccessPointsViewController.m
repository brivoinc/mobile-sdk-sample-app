//
//  AccessPointsViewController.m
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 29/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import "AccessPointsViewController.h"
#import <BrivoOnAir/BrivoOnAir-Swift.h>
#import <BrivoAccess/BrivoAccess-Swift.h>
#import "LabelTableViewCell.h"
#import "LabelHeaderView.h"
#import "UnlockAccessPointViewController.h"

@interface AccessPointsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@end

@implementation AccessPointsViewController
static NSString * const tableViewCellIdentifier = @"LabelTableViewCell";
static NSString * const tableViewHeaderIdentifier = @"LabelHeaderView";

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerNib: [UINib nibWithNibName: tableViewCellIdentifier bundle: nil] forCellReuseIdentifier: tableViewCellIdentifier];
    [self.tableView registerNib: [UINib nibWithNibName: tableViewHeaderIdentifier bundle: nil] forHeaderFooterViewReuseIdentifier: tableViewHeaderIdentifier];

    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.tableView.estimatedSectionHeaderHeight = 44;
    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.brivoOnAirPass.sites[self.selectedSiteIndex].accessPoints.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LabelHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier: tableViewHeaderIdentifier];
    headerView.titleLabel.text = self.brivoOnAirPass.sites[self.selectedSiteIndex].siteName;
    return headerView;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    LabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: tableViewCellIdentifier];
    BrivoSite *site = self.brivoOnAirPass.sites[self.selectedSiteIndex];
    NSArray<BrivoAccessPoint *> *accessPoints = site.accessPoints;
    BrivoAccessPoint *accessPoint = accessPoints[indexPath.row];
    cell.titleLabel.text = accessPoint.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"toUnlockDoorSegue" sender: self];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UnlockAccessPointViewController *destination = (UnlockAccessPointViewController *)segue.destinationViewController;
    BrivoAccessPoint *accessPoint = self.brivoOnAirPass.sites[self.selectedSiteIndex].accessPoints[self.selectedIndexPath.row];
    NSInteger accessPointId = accessPoint.id;
    if (accessPointId == NSNotFound) {
        return;
    }
    NSString *userId = self.brivoOnAirPass.brivoOnairPassCredentials.userId;
    BrivoSelectedAccessPoint *selectedAccessPoint = [[BrivoSelectedAccessPoint alloc] initWithAccessPointId: accessPointId userId: userId readerUid: accessPoint.bluetoothReader.readerUid bleCredentials: self.brivoOnAirPass.bleCredential timeframe: self.brivoOnAirPass.bleAuthTimeFrame passId: self.brivoOnAirPass.passId brivoApiTokens: self.brivoOnAirPass.brivoOnairPassCredentials.tokens];
    destination.selectedAccessPoint = selectedAccessPoint;
}
@end
