//
//  UnlockAccessPointViewController.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 29/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class BrivoSelectedAccessPoint;
@interface UnlockAccessPointViewController : BaseViewController
@property (strong, nonatomic) BrivoSelectedAccessPoint *selectedAccessPoint;
@end

NS_ASSUME_NONNULL_END
