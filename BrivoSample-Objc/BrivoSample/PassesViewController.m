//
//  PassesViewController.m
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 28/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import "PassesViewController.h"
#import <BrivoCore/BrivoCore-Swift.h>
#import <BrivoAccess/BrivoAccess-Swift.h>
#import <BrivoOnAir/BrivoOnAir-Swift.h>
#import "Constants.h"
#import "LabelTableViewCell.h"
#import "LabelHeaderView.h"
#import "AccessPointsViewController.h"

extern struct ConstantsStruct const Constants;

@interface PassesViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<BrivoOnairPass *> *passes;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@end

@implementation PassesViewController
NSString * const tableViewCellIdentifier = @"LabelTableViewCell";
NSString * const tableViewHeaderIdentifier = @"LabelHeaderView";

- (void)viewDidLoad {
    [super viewDidLoad];

    NSError *error;

    BrivoConfiguration* brivoConfiguration = [[BrivoConfiguration alloc] initWithClientId:Constants.CLIENT_ID clientSecret:Constants.CLIENT_SECRET useSDKStorage:YES error: &error];
    [[BrivoSDK instance] configureWithBrivoConfiguration:brivoConfiguration];


    [self.tableView registerNib: [UINib nibWithNibName: tableViewCellIdentifier bundle: nil] forCellReuseIdentifier: tableViewCellIdentifier];
    [self.tableView registerNib: [UINib nibWithNibName: tableViewHeaderIdentifier bundle: nil] forHeaderFooterViewReuseIdentifier: tableViewHeaderIdentifier];

    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.tableView.estimatedSectionHeaderHeight = 44;
    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.passes = [NSArray array];

    self.title = [NSString stringWithFormat: @"BrivoSDK %@", BrivoSDK.sdkVersion];

    [self getBrivoOnAirPasses];
}

- (IBAction)addButtonClicked:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Redeem a pass" message:@"Enter the received data" preferredStyle: UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        [textField setPlaceholder: @"Pass ID"];
        [textField setSecureTextEntry: false];
    }];

    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        [textField setPlaceholder: @"Pass code"];
        [textField setSecureTextEntry: false];
    }];

    __weak PassesViewController *weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf redeemPass:alertController.textFields[0].text : alertController.textFields[1].text];
    }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style: UIAlertActionStyleDestructive handler: nil];

    [alertController addAction: okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: true completion: nil];
}


- (void)redeemPass: (NSString *)passId :(NSString *)passCode {
    NSError *brivoSDKOnAirError;
    __weak PassesViewController *weakSelf = self;
    [[BrivoSDKOnAir instanceAndReturnError: &brivoSDKOnAirError] redeemPassWithPassId: passId passCode: passCode onSuccess:^(BrivoOnairPass * _Nullable pass) {
        [weakSelf getBrivoOnAirPasses];
    } onFailure:^(BrivoError * _Nullable error) {
        NSString *statusCodeString = [NSString stringWithFormat: @"%ld", (long)error.statusCode];
        [weakSelf showAlertWith: statusCodeString : error.errorDescription];
    }];
}

- (void)getBrivoOnAirPasses {
    NSError *brivoSDKOnAirError;

    __weak PassesViewController *weakSelf = self;
    [[BrivoSDKOnAir instanceAndReturnError: &brivoSDKOnAirError] retrieveSDKLocallyStoredPassesOnSuccess:^(NSArray<BrivoOnairPass *> * _Nonnull passes) {
        weakSelf.passes = passes;
        [weakSelf.tableView reloadData];
    } onFailure:^(BrivoError * _Nullable error) {

    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.passes.count;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.passes[section].sites.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LabelHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier: tableViewHeaderIdentifier];
    headerView.titleLabel.text = self.passes[section].passId;
    return headerView;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    LabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: tableViewCellIdentifier];
    cell.titleLabel.text = self.passes[indexPath.section].sites[indexPath.row].siteName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"toPassScreenSegue" sender: self];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass: [AccessPointsViewController class]]) {
        AccessPointsViewController *destination = (AccessPointsViewController *)segue.destinationViewController;
        destination.brivoOnAirPass = self.passes[self.selectedIndexPath.section];
        destination.selectedSiteIndex = self.selectedIndexPath.row;
    }
    
}
@end
