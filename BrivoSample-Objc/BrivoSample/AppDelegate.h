//
//  AppDelegate.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 28/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow * window;


@end

