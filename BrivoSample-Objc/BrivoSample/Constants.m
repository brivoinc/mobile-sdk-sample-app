//
//  Constants.m
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 28/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

const struct ConstantsStruct Constants = {
    .CLIENT_ID = @"CLIENT_ID",
    .CLIENT_SECRET = @"CLIENT_SECRET"
};
