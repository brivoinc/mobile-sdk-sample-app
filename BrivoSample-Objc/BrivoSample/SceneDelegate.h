//
//  SceneDelegate.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 28/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

