//
//  Constants.h
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 28/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

struct ConstantsStruct {
    __unsafe_unretained NSString * const CLIENT_ID;
    __unsafe_unretained NSString * const CLIENT_SECRET;
};

#endif /* Constants_h */
