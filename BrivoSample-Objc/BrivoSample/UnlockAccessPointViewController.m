//
//  UnlockAccessPointViewController.m
//  BrivoSample-Objc
//
//  Created by Dorel MACRA on 29/10/2019.
//  Copyright © 2019 -. All rights reserved.
//

#import "UnlockAccessPointViewController.h"
#import <BrivoAccess/BrivoAccess-Swift.h>
#import <BrivoCore/BrivoCore-Swift.h>
#import <BrivoBLE/BrivoBLE.h>
#import "Pulsator-Swift.h"

@interface UnlockAccessPointViewController ()
@property (weak, nonatomic) IBOutlet UIButton *openUIButton;
@property (weak, nonatomic) IBOutlet UILabel *magicButtonDescriptionLabel;
@property (strong, nonatomic) Pulsator *pulsator;
@property (strong, nonatomic) NSTimer *timer;
@end

@implementation UnlockAccessPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.magicButtonDescriptionLabel setHidden: self.selectedAccessPoint != nil];
    self.pulsator = [Pulsator new];
    [self setupPulsator];
    [self.openUIButton.layer.superlayer addSublayer: self.pulsator];
}

- (IBAction)openButtonClicked:(UIButton *)sender {
    [self.openUIButton setEnabled: false];
    [self.pulsator start];

    NSString *accessPointId = [NSString stringWithFormat:@"%ld", (long)self.selectedAccessPoint.accessPointId];

    [self setUnlockedTimer];
    __weak UnlockAccessPointViewController *weakSelf = self;

    CancellationSignal *cancellationSignal = [[CancellationSignal alloc] init];
    NSTimer *timer = [NSTimer timerWithTimeInterval: 30 repeats: false block:^(NSTimer * _Nonnull timer) {
        cancellationSignal.isCancelled = true;
        [weakSelf resetToInitialState];
        [timer invalidate];
    }];

    if (self.selectedAccessPoint != nil) {
        [[BrivoSDKAccess instance] unlockAccessPointWithPassId: self.selectedAccessPoint.passId accessPointId: accessPointId onSuccess:^{
            [weakSelf setLocked: false];
            [weakSelf.pulsator stop];
        } onFailure:^(BrivoError * _Nonnull error) {
            [weakSelf resetToInitialState];
            NSString *statusCodeString = [NSString stringWithFormat: @"%ld", (long)error.statusCode];
            [weakSelf showAlertWith: statusCodeString : error.errorDescription];
        } cancellationSignal: cancellationSignal];
    } else {
        [[BrivoSDKAccess instance] unlockNearestBLEAccessPointOnSuccess:^{
            [weakSelf setLocked: false];
            [weakSelf.pulsator stop];
        } onFailure:^(BrivoError * _Nonnull error) {
            [weakSelf resetToInitialState];
            NSString *statusCodeString = [NSString stringWithFormat: @"%ld", (long)error.statusCode];
            [weakSelf showAlertWith: statusCodeString : error.errorDescription];
        } cancellationSignal: cancellationSignal];
    }
    [NSRunLoop.currentRunLoop addTimer: timer forMode: NSRunLoopCommonModes];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    self.pulsator.position = self.openUIButton.layer.position;
}

- (void) resetToInitialState {
    [self setLocked: true];
    [self.openUIButton setEnabled: true];
    [self.pulsator stop];
}

- (void) setupPulsator {
    self.pulsator.backgroundColor = [UIColor colorWithRed:0 green:0.455 blue:0.756 alpha: 1].CGColor;
    self.pulsator.numPulse = 3;
    self.pulsator.animationDuration = 2;
    self.pulsator.radius = self.pulsator.radius * 2;
}

- (void) setLocked :(BOOL) isLocked {
    NSString *imageName = isLocked ? @"lock" : @"lock_open";
    UIImage *image = [UIImage imageNamed: imageName];
    [self.openUIButton setBackgroundImage: image forState: UIControlStateNormal];
}

- (void) setUnlockedTimer {
    __weak UnlockAccessPointViewController *weakSelf = self;
    self.timer = [NSTimer timerWithTimeInterval: 30 repeats: false block:^(NSTimer * _Nonnull timer) {
        [weakSelf setLocked: true];
        [weakSelf.openUIButton setEnabled: true];
        [timer invalidate];
    }];
    [NSRunLoop.currentRunLoop addTimer: self.timer forMode: NSRunLoopCommonModes];
}
@end
