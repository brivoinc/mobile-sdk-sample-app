# How to install BrivoMobilePassSDK
## Cocoapods

In your pod file, after 
```
  use_frameworks!
```
You should add
```
    pod 'BrivoMobilePassSDK', :source => 'https://bitbucket.org/brivoinc/mobile-sdk-framework-spec.git'
```
After adding the line above, your pod file should look something like this:
```
target 'YourApplicationName' do
  use_frameworks!

  pod 'BrivoMobilePassSDK', :source => 'https://bitbucket.org/brivoinc/mobile-sdk-framework-spec.git'
end
```
## Manual installation
If you don't want to use cocoapods, you can grab the frameworks from the following url:
```
https://bitbucket.org/brivoinc/mobile-sdk-framework-dist/src/v1.3.0/
```
This sample app was build for framework version 1.3.0

## Documentation
The documentation for the sdk and the license can be found at the following url: 
```
https://bitbucket.org/brivoinc/mobile-sdk-framework-dist/src/v1.3.0/
```
