//
//  Constants.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import Foundation

struct Constants {
    static let CLIENT_ID = "CLIENT_ID"
    static let CLIENT_SECRET = "CLIENT_SECRET"
}
