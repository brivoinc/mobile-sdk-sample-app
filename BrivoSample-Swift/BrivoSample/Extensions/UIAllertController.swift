//
//  UIAllertController.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//
import UIKit

extension UIAlertController {
    class func cancelAlertControllerWith(title: String? = nil,
                                         message: String?,
                                         actionHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
            actionHandler?(action)
        }
        alertController.addAction(cancelAction)
        return alertController
    }
}
