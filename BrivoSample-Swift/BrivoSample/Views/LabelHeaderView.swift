//
//  LabelHeaderView.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit

class LabelHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var label: UILabel!
    
}
