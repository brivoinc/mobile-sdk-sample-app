//
//  LabelImageCell.swift
//  BrivoSample
//
//  Created by Rares Ferastauaru on 9/16/19.
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit

class LabelImageCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
}
