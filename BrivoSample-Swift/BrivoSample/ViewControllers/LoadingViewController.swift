//
//  LoadingViewController.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    private let animationDuration: TimeInterval = 0.3

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

        setUpPresentationStyle()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setUpPresentationStyle()
    }

    private func setUpPresentationStyle() {
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }

    func show(_ show: Bool) {
        if show == true {
            self.show(animated: true)
        } else {
            self.hide(animated: true)
        }
    }

    private func show(animated: Bool, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            guard let window = appDelegate?.window else { return }
            window.addSubview(self.view)
            self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.frame = window.bounds

            if animated == true {
                self.view.alpha = 0
                UIView.animate(withDuration: self.animationDuration, animations: {[weak self] in
                    self?.view.alpha = 1.0
                    }, completion: {(finished) in
                        if finished {
                            completion?()
                        }
                })
            } else {
                self.view.alpha = 1.0
            }
        }
    }

    private func hide(animated: Bool, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            if animated == true {
                UIView.animate(withDuration:self.animationDuration,
                               animations: {[weak self] in
                                self?.view.alpha = 0
                    },
                               completion: {[weak self] (finished) in
                                    if finished {
                                        self?.view.removeFromSuperview()
                                        completion?()
                                    }
                })
            } else {
                self.view.removeFromSuperview()
            }
        }
    }

}
