//
//  ViewController.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit
import BrivoCore
import BrivoBLE
import BrivoOnAir
import BrivoAccess

class UnlockAccessPointViewController: BaseViewController {

    @IBOutlet weak var openUIButton: UIButton!
    @IBOutlet weak var magicButtonDescriptionLabel: UILabel!
    
    var selectedAccessPoint: BrivoSelectedAccessPoint?

    private var pulsator = Pulsator()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPulsator()
        openUIButton.layer.superlayer?.addSublayer(pulsator)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        pulsator.position = self.openUIButton.layer.position
        
        magicButtonDescriptionLabel.isHidden = selectedAccessPoint != nil
    }

    @IBAction func openButtonClicked(_ sender: Any) {
        openUIButton.isEnabled = false
        pulsator.start()
        setUnlockedTimer()

        let cancellationSignal = CancellationSignal()
         let timer = Timer(timeInterval: 30.0, repeats: false) {[weak self] (timer) in
             cancellationSignal.isCancelled = true
             self?.resetToInitialState()
             timer.invalidate()
         }
        
        let brivoSDKAccess = BrivoSDKAccess.instance()
        if let selectedAccessPoint = selectedAccessPoint, let passId = selectedAccessPoint.passId {
            let accessPointIdString = "\(selectedAccessPoint.accessPointId)"
            brivoSDKAccess.unlockAccessPoint(passId: passId, accessPointId: accessPointIdString, onSuccess: {[weak self] in
                timer.invalidate()
                self?.setLocked(isLocked: false)
                self?.pulsator.stop()
            }, onFailure: {[weak self] (brivoError) in
                timer.invalidate()
                self?.resetToInitialState()
                self?.showError(statusCode: brivoError.statusCode, errorName: nil, errorMessage: brivoError.errorDescription)
            }, cancellationSignal: cancellationSignal)
        } else {
            brivoSDKAccess.unlockNearestBLEAccessPoint(onSuccess: {[weak self] in
                timer.invalidate()
                self?.setLocked(isLocked: false)
                self?.pulsator.stop()
            }, onFailure: {[weak self] (brivoError) in
                timer.invalidate()
                self?.resetToInitialState()
                self?.showError(statusCode: brivoError.statusCode, errorName: nil, errorMessage: brivoError.errorDescription)
            }, cancellationSignal: cancellationSignal)
        }
        RunLoop.current.add(timer, forMode: .common)
    }

    private func resetToInitialState() {
        setLocked(isLocked: true)
        openUIButton.isEnabled = true
        pulsator.stop()
    }

    private func setupPulsator() {
        pulsator.backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 1).cgColor
        pulsator.numPulse = 3
        pulsator.animationDuration = 2
        pulsator.radius = pulsator.radius * 2;
    }
    
    private func setLocked(isLocked: Bool) {
        let imageName = isLocked ? "lock" : "lock_open"
        let image = UIImage(named: imageName)
        openUIButton.setBackgroundImage(image, for: .normal)
    }
    
    private func setUnlockedTimer() {
        let timer = Timer(timeInterval: 30.0, repeats: false) {[weak self] (timer) in
            self?.setLocked(isLocked: true)
            self?.openUIButton.isEnabled = true
            timer.invalidate()
        }
        RunLoop.current.add(timer, forMode: .common)
    }
}
