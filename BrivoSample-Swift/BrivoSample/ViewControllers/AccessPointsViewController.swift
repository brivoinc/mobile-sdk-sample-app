//
//  ViewController.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit
import BrivoAccess
import BrivoCore
import BrivoOnAir

class AccessPointsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let cellIdentifier = "LabelImageCell"
    private let headerIdentifier = "LabelHeaderView"
    
    var selectedIndexPath: IndexPath!
    var brivoOnAirPass: BrivoOnairPass!
    var selectedSiteIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.register(UINib(nibName: headerIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.estimatedSectionHeaderHeight = 44
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? UnlockAccessPointViewController
        
        if let accessPoint = brivoOnAirPass.sites?[selectedSiteIndex].accessPoints?[selectedIndexPath.row] {
            let accessPointId = accessPoint.id
            if accessPointId == NSNotFound {
                return
            }
            guard let userId = brivoOnAirPass.brivoOnairPassCredentials?.userId else { return }
            
            let selectedAccessPoint = BrivoSelectedAccessPoint(accessPointId: accessPointId,
                                                          userId: userId,
                                                          readerUid: accessPoint.bluetoothReader?.readerUid,
                                                          bleCredentials: brivoOnAirPass.bleCredential,
                                                          timeframe: brivoOnAirPass.bleAuthTimeFrame,
                                                          passId: brivoOnAirPass.passId,
                                                          brivoApiTokens: brivoOnAirPass.brivoOnairPassCredentials?.tokens)
            destination?.selectedAccessPoint = selectedAccessPoint
        }
    }
}

extension AccessPointsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brivoOnAirPass.sites?[selectedSiteIndex].accessPoints?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        if let cell = cell as? LabelImageCell {
            let accessPoint = brivoOnAirPass.sites?[selectedSiteIndex].accessPoints?[indexPath.row]
            cell.label.text = accessPoint?.name
        }
        
        return cell
    }
}

extension AccessPointsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if let headerView = headerView as? LabelHeaderView {
            headerView.label.text = brivoOnAirPass.sites?[selectedSiteIndex].siteName
        }

        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        performSegue(withIdentifier: "toUnlockDoorSegue", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
