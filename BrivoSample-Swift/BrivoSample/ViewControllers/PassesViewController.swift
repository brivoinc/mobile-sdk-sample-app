//
//  ViewController.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit
import BrivoCore
import BrivoAccess
import BrivoOnAir

class PassesViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!

    @IBAction func addButtonClicked(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Redeem a pass", message: "Enter the received data", preferredStyle: .alert)
        alert.addTextField { (textfield) in
            textfield.placeholder = "Pass ID"
            textfield.isSecureTextEntry = false
        }
        alert.addTextField { (textfield) in
            textfield.placeholder = "Pass code"
            textfield.isSecureTextEntry = false
        }
        let okAction = UIAlertAction(title: "OK", style: .default) {[weak self, weak alert] (action) in
            do {
                self?.isBusy = true
                try BrivoSDKOnAir.instance().redeemPass(passId: alert?.textFields?[0].text ?? "",
                                          passCode: alert?.textFields?[1].text ?? "",
                                          onSuccess: { [weak self] (_) in
                                            self?.isBusy = false
                                            self?.getBrivoOnAirPasses()
                    }) {[weak self] (brivoError) in
                        self?.isBusy = false
                        self?.showError(statusCode: brivoError?.statusCode, errorName: "Error" , errorMessage: brivoError?.errorDescription, actionHandler: nil)
                }
            } catch let error {
                self?.showAlert(title: "Brivo configuration error", message: error.localizedDescription)
            }
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    private let cellIdentifier = "LabelImageCell"
    private let headerIdentifier = "LabelHeaderView"

    private var brivoOnAirPasses: [BrivoOnairPass] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    private var selectedPassIndex: Int = 0
    private var selectedSiteIndex: Int = 0
    private var refreshTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            let brivoConfiguration = try BrivoConfiguration(clientId: Constants.CLIENT_ID,
                                                            clientSecret: Constants.CLIENT_SECRET,
                                                            useSDKStorage: true)
            BrivoSDK.instance.configure(brivoConfiguration: brivoConfiguration)
        } catch let error {
            showAlert(title: "Brivo configuration error", message: error.localizedDescription)
        }

        getBrivoOnAirPasses()
        
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.register(UINib(nibName: headerIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)

        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension

        tableView.estimatedSectionHeaderHeight = 44
        tableView.sectionHeaderHeight = UITableView.automaticDimension

        tableView.dataSource = self
        tableView.delegate = self

        title = "BrivoSDK \(BrivoSDK.sdkVersion)"
    }
    
    private func getBrivoOnAirPasses() {
        do {
            try BrivoSDKOnAir.instance().retrieveSDKLocallyStoredPasses(onSuccess: { [weak self] (brivoOnAirPasses) in
                self?.brivoOnAirPasses = brivoOnAirPasses
            }, onFailure: {[weak self] (brivoError) in
                self?.showError(statusCode: brivoError?.statusCode, errorName: "Error", errorMessage: brivoError?.errorDescription)
            })
        } catch let error {
            showAlert(title: error.localizedDescription, message: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        refreshTimer = Timer(fire: Date(), interval: 15.0, repeats: true) { [weak self] (timer) in
            self?.refreshPasses()
        }
        RunLoop.current.add(refreshTimer, forMode: .common)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        refreshTimer.invalidate()
    }

    private func refreshPasses() {
        for (index, brivoOnAirPass) in brivoOnAirPasses.enumerated() {
            do {
                if let tokens = brivoOnAirPass.brivoOnairPassCredentials?.tokens {
                    try BrivoSDKOnAir.instance().refreshPass(brivoTokens: tokens, onSuccess: {[weak self] (refreshedPass) in
                        if let refreshedPass = refreshedPass {
                            self?.brivoOnAirPasses[index] = refreshedPass
                        } else {
                            self?.brivoOnAirPasses.remove(at: index)
                        }
                    }) {[weak self] (responseStatus) in
                        self?.isBusy = false
                        self?.showError(statusCode: responseStatus?.statusCode, errorName: "Error", errorMessage: responseStatus?.errorDescription, actionHandler: nil)
                    }
                }
            } catch let error {
                showAlert(title: error.localizedDescription, message: nil)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? AccessPointsViewController
        destination?.brivoOnAirPass = brivoOnAirPasses[selectedPassIndex]
        destination?.selectedSiteIndex = selectedSiteIndex
    }
}

extension PassesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return brivoOnAirPasses.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < brivoOnAirPasses.count else { return 0}
        return brivoOnAirPasses[section].sites?.count ?? 0
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)

        if let cell = cell as? LabelImageCell {
            let site = brivoOnAirPasses[indexPath.section].sites?[indexPath.row]
            cell.label.text = site?.siteName
        }

        return cell
    }
}

extension PassesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if let headerView = headerView as? LabelHeaderView {
            headerView.label.text = brivoOnAirPasses[section].passId
        }

        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPassIndex = indexPath.section
        selectedSiteIndex = indexPath.row
        performSegue(withIdentifier: "toPassScreenSegue", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
