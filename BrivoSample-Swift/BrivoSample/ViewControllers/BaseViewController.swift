//
//  BaseViewController.swift
//  BrivoSample
//
//  Copyright © 2019 Brivo. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var isBusy: Bool = false {
        didSet {
            loadingViewController.show(isBusy)
        }
    }

    private let loadingViewController = LoadingViewController.init(nibName: "LoadingViewController", bundle: nil)

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.modalPresentationStyle = .fullScreen
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .fullScreen
    }
    
    func showAlert(title: String?, message: String?, actionHandler: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController.cancelAlertControllerWith(title: title, message: message, actionHandler: actionHandler)
        present(alertController, animated: true)
    }
    
    func showError(statusCode: Int?, errorName: String?, errorMessage: String?, actionHandler: ((UIAlertAction) -> Void)? = nil) {
        var title = "Error"
        if let statusCode = statusCode {
            title += " \(statusCode)"
        }
        if let errorName = errorName {
            title += " \(errorName)"
        }
        showAlert(title: title, message: errorMessage, actionHandler: actionHandler)
    }
}
